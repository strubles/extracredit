import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Main {
    private static String input = "";
    private static boolean labelIsResult = false;
    private static JLabel label = new JLabel(input, SwingConstants.CENTER);

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                //set UI style to the native OS's
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                    e.printStackTrace(); }

                JFrame frame = new JFrame("Calc");
                frame.setMinimumSize(new Dimension(230,310));

//                ImageIcon pic = new ImageIcon("calc.ico");
//                frame.setIconImage(pic.getImage());

                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //terminate when X is clicked

                JPanel pan = new JPanel(new GridBagLayout());



                //create and configure all buttons...
                JButton add = new JButton(new AddAction());
                add.setFont(new Font("Segoe UI", Font.BOLD, 20));

                JButton sub = new JButton(new SubAction());
                sub.setFont(new Font("Segoe UI", Font.BOLD, 20));

                JButton mul = new JButton(new MulAction());
                mul.setFont(new Font("Segoe UI", Font.BOLD, 20));

                JButton div = new JButton(new DivAction());
                div.setFont(new Font("Segoe UI", Font.BOLD, 20));

                JButton equal = new JButton(new EqAction());
                equal.setFont(new Font("Segoe UI", Font.BOLD, 20));

                JButton dot = new JButton(new DotAction());
                dot.setFont(new Font("Segoe UI", Font.BOLD, 20));

                JButton zero = new JButton(new ZeroAction());
                zero.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton one = new JButton(new OneAction());
                one.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton two = new JButton(new TwoAction());
                two.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton three = new JButton(new ThreeAction());
                three.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton four = new JButton(new FourAction());
                four.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton five = new JButton(new FiveAction());
                five.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton six = new JButton(new SixAction());
                six.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton seven = new JButton(new SevenAction());
                seven.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton eight = new JButton(new EightAction());
                eight.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton nine = new JButton(new NineAction());
                nine.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton clear = new JButton(new ClearAction());
                clear.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                JButton backspc = new JButton(new BackAction());
                backspc.setFont(new Font("Segoe UI", Font.PLAIN, 20));

                label.setOpaque(true);
                label.setFont(new Font("Segoe UI", Font.PLAIN, 20));
                label.setPreferredSize(new Dimension(50,50));

                pan.add(label, new GridBagConstraints(
                        0,0,4,1,0.5,0.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(5,15,5,15), 1, 1));

                //top row
                pan.add(seven, new GridBagConstraints(
                        0, 1, 1, 1, 0.5, 0.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2, 8, 2, 2), 1, 1));
                pan.add(eight, new GridBagConstraints(
                        1, 1, 1, 1, .5, .5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(nine, new GridBagConstraints(
                        2,1,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(add, new GridBagConstraints(
                        3,1,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,8), 1, 1));

                //second row
                pan.add(four, new GridBagConstraints(
                        0,2,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,8,2,2), 1, 1));
                pan.add(five, new GridBagConstraints(
                        1,2,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(six, new GridBagConstraints(
                        2,2,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(sub, new GridBagConstraints(
                        3,2,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,8), 1, 1));

                //third row
                pan.add(one, new GridBagConstraints(
                        0,3,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,8,2,2), 1, 1));
                pan.add(two, new GridBagConstraints(
                        1,3,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(three, new GridBagConstraints(
                        2,3,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(mul, new GridBagConstraints(
                        3,3,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,8), 1, 1));

                //fourth row
                pan.add(clear, new GridBagConstraints(
                        0,4,1,1,0.5,0.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,8,2,2), 1, 1));
                pan.add(zero, new GridBagConstraints(
                        1,4,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(dot, new GridBagConstraints(
                        2,4,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,2), 1, 1));
                pan.add(div, new GridBagConstraints(
                        3,4,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,2,8), 1, 1));

                //fifth row
                pan.add(backspc, new GridBagConstraints(
                        0,5,1,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,8,6,2),1,1));

                pan.add(equal, new GridBagConstraints(
                        1,5,3,1,.5,.5,
                        GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                        new Insets(2,2,6,8), 1, 1));

                frame.add(pan);
                frame.setSize(new Dimension(230, 310));
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            } //end run()
        }); //end new Runnable()
    } //end main()

    private static class ZeroAction extends AbstractAction {
        public ZeroAction() {
            putValue(AbstractAction.NAME, "0");
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            //if displaying a result, replace it
            if (labelIsResult){
                input = "0";
            }

            String num2str = "";
            int operatorAt = findOperator(input);
            if (operatorAt != -1){
                num2str = (input.substring(operatorAt+1)).trim();} //everything after operator is 2nd num

            //prevent inputs such as "000"
            //for first op
            if ((! input.equals("0")) && (operatorAt == -1)){
                input = input + "0";
                label.setText(input);
                labelIsResult = false;
                return;
                }

            //for second op
            else if (!num2str.equals("0")){
                input = input + "0";
            }


            label.setText(input);
            labelIsResult = false;
        }
    }

    private static class OneAction extends AbstractAction {
        public OneAction() {
            putValue(AbstractAction.NAME, "1");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            //replace an input of just 0 or a result with 1
            if (input.equals("0") || labelIsResult){
                input = "1";
            }
            //replace a second operand of just 0 with 1
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "1";
                }
                else {input = input + "1";}
            }
            //otherwise just add 1 at the end like normal
            else{
                input = input + "1";}
            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class TwoAction extends AbstractAction {
        public TwoAction() {
            putValue(AbstractAction.NAME, "2");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input = "2";
            }
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "2";
                }
                else {input = input + "2";}
            }
            else{
                input = input + "2";}

            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class ThreeAction extends AbstractAction {
        public ThreeAction() {
            putValue(AbstractAction.NAME, "3");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input = "3";
            }
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "3";
                }
                else {input = input + "3";}
            }
            else{
                input = input + "3";}

            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class FourAction extends AbstractAction {
        public FourAction() {
            putValue(AbstractAction.NAME, "4");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input = "4";
            }
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "4";
                }
                else {input = input + "4";}
            }
            else{
                input = input + "4";}

            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class FiveAction extends AbstractAction {
        public FiveAction() {
            putValue(AbstractAction.NAME, "5");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input = "5"; }
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "5";
                }
                else {input = input + "5";}
            }
            else{ input = input + "5";}

            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class SixAction extends AbstractAction {
        public SixAction() {
            putValue(AbstractAction.NAME, "6");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input="6";}
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "6";
                }
                else {input = input + "6";}
            }
            else{ input = input + "6";}
            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class SevenAction extends AbstractAction {
        public SevenAction() {
            putValue(AbstractAction.NAME, "7");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input = "7";}
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "7";
                }
                else {input = input + "7";}
            }
            else {input = input + "7";}
            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class EightAction extends AbstractAction {
        public EightAction() {
            putValue(AbstractAction.NAME, "8");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input = "8";}
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "8";
                }
                else {input = input + "8";}
            }
            else{ input = input + "8";}
            label.setText(input);
            labelIsResult = false;
        }
    }
    private static class NineAction extends AbstractAction {
        public NineAction() {
            putValue(AbstractAction.NAME, "9");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int operatorAt = findOperator(input);

            if (input.equals("0") || labelIsResult){
                input = "9"; }
            else if(operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("0")){
                    input = input.substring(0, input.length()-1) + "9";
                }
                else {input = input + "9";}
            }
            else {input = input + "9";}

            label.setText(input);
            labelIsResult = false;
        }
    }

    private static class AddAction extends AbstractAction {
        public AddAction() {
            putValue(AbstractAction.NAME, "+");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if ((! input.equals("")) && !(input.contains("+") || input.contains(" - ") ||
                    input.contains("/") || input.contains("*")) //no operator yet
                    && !(Character.toString(input.charAt(input.length()-1)).equals("-")) //and not immediately after negate
                    && !(Character.toString(input.charAt(input.length()-1)).equals("."))){ //and not immediately after dot
                input = input + " + ";}
            label.setText(input);
            labelIsResult = false;
        }
    }

    private static class SubAction extends AbstractAction {
        public SubAction() {
            putValue(AbstractAction.NAME, "-");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
//            if ((! input.equals("")) && !(input.contains("+") || input.contains(" - ") ||
//                    input.contains("/") || input.contains("*"))){
//                input = input + " - ";}
//            else{ input = input + " -";}


            //negate
            if (input.equals("")){ input = "-";}

            //negate
            else if ((input.contains(" - ") || input.contains("+") ||
                    input.contains("*") || input.contains("/"))          //already has operator
                    && !Character.isDigit(input.charAt(input.length()-1))   //AND not immediately after a digit
                    && !(Character.toString(input.charAt(input.length()-1)).equals("-"))//AND not immediately after a negate
                    && !(Character.toString(input.charAt(input.length()-1)).equals("."))){ //AND not immediately after a dot
                input = input + " -";
            }

            //minus
            else if (!(input.contains(" - ") || input.contains("+") ||
                    input.contains("*") || input.contains("/"))         // doesn't already have operator
                    && !(Character.toString(input.charAt(input.length()-1)).equals("-")) //AND not immediately after negate
                    && !(Character.toString(input.charAt(input.length()-1)).equals("."))) //AND not immediately after dot
            {
                input = input + " - ";
            }
            label.setText(input);
            labelIsResult = false;


        }
    }
    private static class MulAction extends AbstractAction {
        public MulAction() {
            putValue(AbstractAction.NAME, "*");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if ((! input.equals("")) && !(input.contains("+") || input.contains(" - ") ||
                    input.contains("/") || input.contains("*"))
                    && !(Character.toString(input.charAt(input.length()-1)).equals("-")) //not immediately after negate
                    && !(Character.toString(input.charAt(input.length()-1)).equals("."))){ //not immediately after dot
                input = input + " * ";}
            label.setText(input);
            labelIsResult = false;

        }
    }
    private static class DivAction extends AbstractAction {
        public DivAction() {
            putValue(AbstractAction.NAME, "/");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if ((! input.equals("")) && !(input.contains("+") || input.contains(" - ") ||
                    input.contains("/") || input.contains("*"))
                    && !(Character.toString(input.charAt(input.length()-1)).equals("-")) //not immediately after negate
                    && !(Character.toString(input.charAt(input.length()-1)).equals("."))){ //not immediately after dot
                input = input + " / ";}
            label.setText(input);
            labelIsResult = false;
        }
    }

    private static class EqAction extends AbstractAction {
        public EqAction() {
            putValue(AbstractAction.NAME, "=");
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            int operatorAt = findOperator(input);
            if (operatorAt == -1) {return;}


            String num1str = (input.substring(0, operatorAt)).trim(); //everything before operator is 1st num
            String num2str = (input.substring(operatorAt+1)).trim(); //everything after operator is 2nd num

            float num1fl;
            float num2fl;

            try
            {
                num1fl = Float.parseFloat(num1str);
                num2fl = Float.parseFloat(num2str);
            }
            catch (NumberFormatException nfe)
            {
                return;
            }

            float answer = 0;

            switch (Character.toString(input.charAt(operatorAt))) {
                case "-":
                    answer = num1fl - num2fl;
                    break;
                case "+":
                    answer = num1fl + num2fl;
                    break;
                case "/":
                    if (num2fl == 0) {
                        label.setText("Error: divide by 0");
                        labelIsResult = false;
                        input="";
                        return; }
                    answer = num1fl / num2fl;
                    break;
                case "*":
                    answer = num1fl * num2fl;
                    break;
            }

            input = String.valueOf(answer);

            if (input.endsWith(".0")){
                input = input.substring(0, input.length()-2); }

            label.setText(input);
            labelIsResult = true;
        }
    }

    private static class ClearAction extends AbstractAction {
        public ClearAction(){
            putValue(AbstractAction.NAME, "C");
        }
        @Override
        public void actionPerformed(ActionEvent e){
            input = "";
            label.setText(input);
            labelIsResult = false;
        }
    }

    private static class DotAction extends AbstractAction {
        public DotAction() { putValue(AbstractAction.NAME, ".");}
        @Override
        public void actionPerformed(ActionEvent e){
            if (labelIsResult){
                input = "0.";
                labelIsResult = false;
                label.setText(input);
                return;}

            if (input.length() == 0 || input.equals("-")){
                input = input + "0.";
                label.setText(input);
                return;
            }
            int operatorAt = findOperator(input);

            if (Character.isDigit(input.charAt(input.length()-1))){ //last char is a digit
                if (operatorAt == -1){                  //no operator yet
                    if (!input.contains(".")){
                        input = input + "."; }}
                else{
                    String num2str = (input.substring(operatorAt+1)).trim(); //everything after operator is 2nd num
                    //System.out.println("\"" + num2str + "\"");
                    if (!num2str.contains(".")){
                        input = input + "."; }
                    else if (num2str.equals("-")){
                        input = input + "0.";
                    }
                }
            } //end "if last char is digit"

            else if (operatorAt != -1){
                String num2str = (input.substring(operatorAt+1)).trim();
                if (num2str.equals("") || num2str.equals("-")){
                    input = input + "0.";
                }
            }


            label.setText(input);
        }
    }

    private static class BackAction extends AbstractAction{
        public BackAction(){
            putValue(AbstractAction.NAME, "←");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if(labelIsResult){
                input="";
                labelIsResult = false;
            }
            else if(input.length()>0){
                input = input.substring(0,input.length()-1);}
            label.setText(input);
        }
    }

    private static int findOperator(String str){

        int operatorAt = -1;

        //loop thru string
        String curChar;
        for (int index = 0; index < str.length(); index++) // While not fallen off the string
        {
            curChar = Character.toString(str.charAt(index)); //Current character is the one at this index
            if ("+-/*".contains(curChar)) // If curChar is one of these operators and we haven't already found an operator
            {
                if (! (str.length() <= index+1)){
                    if (Character.isDigit(str.charAt(index+1))){
                        continue; //we found a NEGATE symbol, not a MINUS, so this is not the operator!
                }}
                operatorAt = index; //Operator is at this index
                break;
            }
        }
        return operatorAt;
    } //end findOperator
}
